
The aim of this guide is to explain how card payment behavior and adding extras works.

### Table of Contents
[Card Payment Behaviour](#card-payment-behaviour)<br>
[Adding Extras](#adding-extras)

# Card Payment Behaviour

To understand how card payments work, you need to understand that there is a payment process and the payments go through a series of different states depending on the actions of the customer and their interactions with the booking system and other financial systems.

These are a set of different user stories that you should be aware of.

# User Stories for Card Payments

## Adding a Payment Fails

1. At the beginning of the process the payment is open.
2. The card payment fails.

**Result:** A new payment is able to be added.

## Payment is Successful but Authentication Fails

1. At the beginning of the process the payment is open.
2. The card payment is successful.
3. The payment can be authorised.
4. Authentication fails.

**Result:** The payment is in a failed state.

## Payment and Authentication are Successful but the Final Status is Void.

1. At the beginning of the process the payment is open.
2. The card payment is successful.
3. The payment can be authorised.
4. Authentication succeeds.
5. The payment is voided.

**Result:** The payment is in void state.

## Payment, Authentication and Charge Succeed

1. At the beginning of the process the payment is open.
2. The card payment is successful.
3. The payment can be authorised.
4. Authentication succeeds.
5. The payment is charged.

**Result:** The payment is successful.

## Payment is not authorised within 60 minutes

1. At the beginning of the process the payment is open.
2. The card payment is successful.
3. No authorisation within 60 minutes.

**Result:** The payment expires.


## Payment, Authentication and Charge Succeed but Customer Requests Refund.

1. At the beginning of the process the payment is open.
2. The card payment is successful.
3. The payment can be authorised.
4. Authentication succeeds.
5. The payment is charged.
6. A customer requests a refund.
7. A refund can be repeatedly refunded up to a given refund sum.

**Result:** The refund succeeds or fails.

# Adding Extras

It is possible for customers to add extras to their booking.

## Rules of Extras

It is only possible for customers to order extras when the following rules are followed:

* An order is in paid status and not refunded.
* You cannot have two active extras at once.

# User Story for Extras

## Customer Requests Extras

1. A customer requests extras via Kiwi's website.

2. Kiwi's backend computer system is monitoring the web interface for new extras orders, when a customer communicates with the BookingAPI.

3. The customer makes a new order of extras.

4. Kiwi's backend computer system creates an order and price that the customer can view on the website.

5. The BookingAPI sends the customer an email request to pay.
  * If a customer does not pay within 24 hours the booking will be cancelled.

6. The customer goes to Kiwi's website to pay.
  * If the payment is not within 60 minutes the payment expires.

7. The customer is redirected to the payment gate.

8. The payment gate sends a callback to the BookingAPI with the status: failure or success.
  * If the payment is successful then the BookingAPI processes the extras ordered by the customer.

9. When the processing of the order is finished an email confirmation is sent to the customer via a third party API.
